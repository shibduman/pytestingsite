from flask import (Flask,
session,
redirect,
url_for,
escape,
request,
render_template)

import os, json, importlib


app = Flask(__name__)
app.secret_key = os.urandom(16)

ASSIGNMENT_DIR = "assignments/"
INFO_FILE = "info.json"
FILE_UPLOAD_NAME = "file_to_test"
TEMP_DIR_NAME = "tmp"
SUBMISSION_NAME = "submission.py"
HARNESS_NAME = "harness.py"
GENERATE = "gen_tests"
RESULTS_FILE = "results.json"

assignments_listing = []

def load_module_from_path(mod_path, module_name="test.module"):
	result = None
	try:
		spec = importlib.util.spec_from_file_location(module_name, mod_path)
		result = importlib.util.module_from_spec(spec)
		spec.loader.exec_module(result)
	except Exception as e:
		print("failed to load test module")
		print(e)
		result = str(e)
	finally:
		return result

# @app.route('/')
# def hello_world():
#     return render_template("index.html")

@app.route("/uploadFiles/<path:assignment_name>", methods=["POST"])
def upload_file(assignment_name=None):
	print(FILE_UPLOAD_NAME)
	print(assignment_name)
	req_ip = request.remote_addr
	if FILE_UPLOAD_NAME in request.files and request.files[FILE_UPLOAD_NAME]:
		test_file = request.files[FILE_UPLOAD_NAME]
		#check if it's actually a python file
		if test_file.filename.endswith(".py"):
			#it's an actual py file, save it and run tests

			#we have the assignment name, and the file to test, let's test it
			current_assignment = ASSIGNMENT_DIR + assignment_name
			attempt_number = str(int(len(os.listdir(current_assignment + "/attempts"))) + 1)
			attempt_dir = ASSIGNMENT_DIR + assignment_name + "/" + "attempts/" + attempt_number
			print(attempt_dir)
			if not os.path.exists(attempt_dir):
				os.mkdir(attempt_dir)

			submitted_file_path = attempt_dir + "/" + SUBMISSION_NAME

			#read the submission info then save it as a file
			test_results = {"results": []}
			outputs = []
			test_file.save(submitted_file_path)
			test_results["submission"] = str(open(submitted_file_path).read())


			#load the test harness and the submitted file for the function to test
			harness = load_module_from_path(current_assignment + "/" + HARNESS_NAME)
			test_fn_container = load_module_from_path(submitted_file_path)
			print("file is " + test_results["submission"])
			if harness != None:
				print("all set to test")
				cases = getattr(harness, GENERATE)()
				#run the submission against the generated tests
				for test_case in cases["tests"]:
					print("running test " + str(test_case))
					output = {"passed":False}
					output["input"] = str(test_case["input"])
					output["expected"] = str(test_case["expected"])
					output["show"] = test_case["show"]
					try:
						#@TODO: this is an utter shit way of doing this
						#
						#instead:
						#-load the file for testing into memory
						#-read the function name and arguments
						#-remove the arguments and replace with *args or **kwargs (to read https://www.saltycrane.com/blog/2008/01/how-to-use-args-and-kwargs-in-python/)
						#-store the variable names to submit the arguments with correctly
						#-save the original for reference and save the modified one for testing
						#
						#with the above a submitted function with 3 arguments goes
						#from:
						#someFunction(argOne,argTwo,argThree)
						#to:
						#someFunction(*args)
						#with calling becoming something like someFunction(argOne=inputOne, argTwo=inputTwo, argThree=inputThree)

						#check how many arguments there are and call the function accordingly
						run_result = None
						if isinstance(test_fn_container, str):
							run_result = "Failed to run: " + test_fn_container
						elif cases["args"] == 1:
							run_result = getattr(test_fn_container, cases["desired-function"])(test_case["input"])
						elif cases["args"] == 2:
							run_result = getattr(test_fn_container, cases["desired-function"])(test_case["input"][0], test_case["input"][1])
						elif cases["args"] == 3:
							run_result = getattr(test_fn_container, cases["desired-function"])(test_case["input"][0], test_case["input"][1], test_case["input"][2])

						if run_result == test_case["expected"]:
							output["passed"] = True
							print("test passed")
						else:
							print("test failed")
					except Exception as e:
						print(e)
						output["passed"] = False
						run_result = str(e)

					output["actual"] = str(run_result)
					outputs.append(output)
				test_results["results"] = outputs
				print(test_results)
				with open(attempt_dir + "/" + RESULTS_FILE, 'w') as json_results:
					json.dump(test_results, json_results)
					print("Saved results")

			print(test_results)



		#import the file
		#run the tests from the solution file
		#put the results in the assignment attempts directory
	return redirect(url_for("load_assignment", assignment_name=assignment_name))


@app.route('/')
@app.route("/assignments/")
@app.route("/assignments/<path:assignment_name>")
def load_assignment(assignment_name=None):
	assignment = None
	assignment_path = None
	assignment_data = {}
	dir_assignments = os.listdir(ASSIGNMENT_DIR)

	#navigate to root assignments folder since no specific one was requested
	if assignment_name == None:
		print("no assignment requested, sending to assignments root page")
		assignments_list = []
		for current_assignment in dir_assignments:
			print(current_assignment)
			assignment_map = {}
			assignment_map["name"] = str(current_assignment)
			assignment_map["url"] = "/assignments/" + str(current_assignment)
			assignments_list.append(assignment_map)
		return render_template("all_assignments.html",
			assignment_listing = assignments_list)

	#navigate to a specific assignment since one was requested
	else:
		for directoryName in dir_assignments:
			#found the assignment!
			if directoryName == assignment_name:
				assignment = assignment_name
				assignment_path = ASSIGNMENT_DIR + directoryName
				attempt_path = assignment_path + "/attempts"
				attempt_list = []
				assignment_data = {}
				assignment_data["attempts"] = []

				#every assignment should have an info file
				try:
					with open(assignment_path + "/" + INFO_FILE) as infoJSON:
						assignment_data = json.load(infoJSON)
						assignment_data["thing"] = "thing"
						assignment_data["uploadPath"] = "/uploadFiles/" + assignment_data["routeName"]
						print(assignment_data["description"])
				except Exception as e:
					#failed to find the info file
					print(str(e))
					return "Invalid Assignment"

				#check all the attempts for the current assignment (if any)
				try:
					attempts = os.listdir(attempt_path)
					for attempt in attempts:
						attempt_list.append(int(attempt))

					#python's map function is shit so we have to make do with hand loops
					#to convert items from strings to ints and back so we can sort the attempt numbers
					#@TODO: find out why this map shit is broken and retarded and how we can do this better
					attempt_list.sort()
					attempt_listing = []
					for a_num in attempt_list:
						attempt_info = {}

						# attempt_info["submission"] =
						with open(attempt_path + "/" + str(a_num) + "/" + RESULTS_FILE) as res:
							attempt_info = json.load(res)

						attempt_info["number"] = a_num
						attempt_listing.append(attempt_info)

					assignment_data["attempts"] = attempt_listing
				except Exception as e:
					#there were no attempts on the current assignment
					print("There was an error with the assinment attempts")
					print(e)

				print(assignment_data)
				break



		#we found a specific assignment
		if assignment != None:
		    return render_template("assignment.html",
		    	assignment_data=assignment_data)
		#we found nothing
		else:
			return "Could not find assignment"

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)