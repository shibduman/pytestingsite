import random

def gen_tests():
	result = {"desired-function" : "minAge", "tests" : [], "args":2}

	result["tests"].append({
		"input": [[
		{
		"name":"Bob",
		"age":22
		},
		{
		"name":"NotBob",
		"age":-22
		},
		{
		"name":"Frederick",
		"age":2
		}
		], 30],
		"expected": [],
		"show": True
		})

	result["tests"].append({
		"input": [[
		{
		"name":"Bob",
		"age":22
		},
		{
		"name":"NotBob",
		"age":-22
		},
		{
		"name":"Frederick",
		"age":50
		}
		], 30],
		"expected": ["Frederick"],
		"show": True
		})


	result["tests"].append({
		"input": [[
		{
		"name":"Bob",
		"age":3
		},
		{
		"name":"NotBob",
		"age":2
		},
		{
		"name":"Frederick",
		"age":2
		}
		], 1],
		"expected": ["Bob","Frederick","NotBob"],
		"show": False
		})

	result["tests"].append({
		"input": [[
		{
		"name":"Bob",
		"age":3
		},
		{
		"name":"NotBob",
		"age":20
		},
		{
		"name":"Frederick",
		"age":1
		}
		], 10],
		"expected": ["NotBob"],
		"show": False
		})

	result["tests"].append({
		"input": [[
		{
		"name":"Bob",
		"age":3
		},
		{
		"name":"Frederick",
		"age":100
		},
		{
		"name":"NotBob",
		"age":20
		}
		], 10],
		"expected": ["Frederick","NotBob"],
		"show": False
		})

	result["tests"].append({
		"input": [[], 30],
		"expected": [],
		"show": False
		})

	return result
