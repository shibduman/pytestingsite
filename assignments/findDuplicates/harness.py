import random

def gen_tests():
	result = {"desired-function" : "findDuplicates", "tests" : [], "args":1}

	result["tests"].append({
		"input": [1,2,3,4],
		"expected": set(),
		"show": True
		})

	result["tests"].append({
		"input": [1,1,1,1,2,3,4,5,6,8,8,8],
		"expected": {1,8},
		"show": True
		})

	result["tests"].append({
		"input": ["0", "two", "one", "one"],
		"expected": {"one"},
		"show": True
		})

	result["tests"].append({
		"input": [],
		"expected": set(),
		"show": False
		})

	result["tests"].append({
		"input": [2,4,5,6,7,8,9,20],
		"expected": set(),
		"show": False
		})

	return result


# def solve(array):
# 	result = {}
# 	seen = {}
# 	for x in array:
# 		if x not in seen:
# 			seen.add(x)
# 		elif x in seen:
# 			result.add(x)
# 	return result