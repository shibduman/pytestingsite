# Python Testing Site For Simple TDD Nonsense

Just a small site to set up assignments and let users submit code to run against test cases.

## Getting Started

Set the FLASK_APP variable and then run the app

```
$ FLASK_APP=main.py && flask run
```

That's it so far.
